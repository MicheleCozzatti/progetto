
//funzione per creare matrici
function matrixDimension(n, corpo, nomepagina){
	var sel1= document.getElementsByClassName("SelectRow")[n].value;
	var sel2= document.getElementsByClassName("SelectColumn")[n].value;
	var corpotabella= corpo;
	//finchè la tabella ha figli rimuovili
	while(corpotabella.hasChildNodes()){
		corpotabella.removeChild(corpotabella.firstChild);
	}
	var operazione=nomepagina.text;
	//quando i due select sono diversi di zero, allora
	if(sel1!=0 && sel2!=0){
		//rendi visibile bottone calcola
		document.getElementById("bottonecalcola").style.visibility="visible";
		for(riga=0; riga<sel1; riga++){
			var nuovariga= document.createElement("tr");
			for(col=0; col<sel2; col++){
				var nuovacolonna = document.createElement("td");
				//viene creato input
				var input = document.createElement("INPUT");
				//input tipo testo
				input.setAttribute("type", "text");
				// i valori dell'input vengono inizializzati con il valore 0
				input.setAttribute("value", "0");
				// viene settata la massima lunghezza accettate dall'input
				input.setAttribute("maxlength", 5);
				//viene associato un evento onkeypress che attraverso la funzione isNumber(event) permette di digitare solo numeri, non lettere
				input.setAttribute("onkeypress","return isNumber(event)");
				//aggiungo l'input all'elemento td
				nuovacolonna.appendChild(input);
				//aggiungo td a tr
				nuovariga.appendChild(nuovacolonna);
			}
			//aggiungo tr al corpo della tabella
			corpotabella.appendChild(nuovariga);
		}
		if(operazione=="Prodotto"){
			document.getElementById("righe").style.visibility="visible";
			document.getElementById("colonne").style.visibility="visible";
		}
		//bottonecalcola viene mostrato quando il select è diverso da zero 
		if(n!=0){
			document.getElementById("bottonecalcola").style.visibility="visible";
		}
	}
	//se i select sono uguali a zero allora copri il bottonecalcola
	else{
		document.getElementById("bottonecalcola").style.visibility="hidden";
	}
}; 

// funzione che permette di verificare che alla pressione di un tasto, il valore deve essere numerico
function isNumber(evt){ 
	var charCode;
	if(evt.which) {
		charCode = evt.which; //event.which --> codice numerico che è stato premuto
	}
	else {
		charCode = evt.keyCode; //event.keyCode--> codice Unicode del tasto premuto
	}
	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
		return false;
	}
	else{
		return true;
	}
};

//funzione per estrarre dati dalla matrice
function takeData(nomepagina,corpo, corpo2=null){
	var nome= nomepagina.text;
	var arr=[];
	var newarray=[];
	var miatabella= corpo;
	var numerorighe= miatabella.rows.length;
	for(i=0; i<numerorighe; i++){
		var numerocolonne = miatabella.rows[i].cells.length;
		var arr2=[];
		 for(var j = 0; j < numerocolonne; j++){
			var valorecella= miatabella.rows[i].cells[j].children[0].value;
			//parseFloat ritorna un numero con la virgola o un null 
			arr2.push(parseFloat(valorecella));
		}
		arr.push(arr2);
	}
	//viene creato l'oggetto obj che contiene come chiave operazione e matrice1
	var obj= {operazione: nome, matrice1: arr};
	var myJSONString= JSON.stringify(obj);
	//caso del prodotto, dove le matrici che vengono passate sono due
	if(corpo2!=null){
		var arr=[];
		var newarray=[];
		var miatabella= corpo2;
		var numerorighe= miatabella.rows.length;
		for(i=0; i<numerorighe; i++){
			var numerocolonne = miatabella.rows[i].cells.length;
			var arr2=[];
			 for(var j = 0; j < numerocolonne; j++){
				var valorecella= miatabella.rows[i].cells[j].children[0].value;
				arr2.push(parseFloat(valorecella));
			}
			arr.push(arr2);
		}
		//viene creato obj2 relativo alla seconda matrice (matrice2)
		var obj2= {operazione: nome, matrice2: arr};
		//obj3 non è altro che l'unione tra obj1 e obj2
		var obj3 = Object.assign(obj, obj2);
		var myJSONString3= JSON.stringify(obj3);
		return myJSONString3;
	}
	else{
		return myJSONString;
	}
};

//websocket, questa funzione è richiamata quando viene premuto il tasto calcola
function runWebSocket(n,nomepagina,corpo,corpo2=null) {
	var ws = new WebSocket("ws://127.0.0.1:8765/");
	//salvo nella variabile matrix il risultato della funzione takeData
	var matrix= takeData(nomepagina,corpo,corpo2);
	ws.onerror = function (event) {
		alert("Impossibile connettersi al server, riprovare più tardi");
		console.log("cannot connect to server");
	}
	ws.onopen = function (event) {
		console.log("connected to server");
		ws.send(matrix);
	}
	ws.onmessage = function (event) {
		var result = event.data;
		//una volta ottenuto il risultato richiamo la funzione per disegnare la matrice
		drawMatrix(result, n);
		console.log("received from server: " + event.data);
		ws.close();
	};
}

//disegna la matrice che ritorna dal programma python nel caso del prodotto, trasposta e inversa. Mentre disegna un numero per determinante e rango.
function drawMatrix(input,id){
	var inputJSON= JSON.parse(input);
	var risultato= inputJSON["risultato"];
	//caso errore nell'inserimento dei dati
	if(risultato=="errcontr"){
		alert("Dati non corretti");
	}
	//caso errore, se la matrice nell'inversa ha il determinante uguale a zero
	else if(risultato=="errinv"){
		alert("La matrice ha il determinante uguale a zero");
	}
	// caso errore, quando il JSON non è completo
	else if(risultato=="errJSON"){
		alert("Ops, qualcosa è andato storto... si prega di riavviare la pagina e riprovare");
	}
	//se il risultato è formato da un solo numero, quindi nel caso del determinante e rango
	else if(risultato.toString.length==1){
		document.getElementById(id).innerHTML= risultato;
	}
	//altrimenti
	else{
		//una volta ottenuto il JSON in python, viene fatta una richiesta XMLHttpRequest a un programma php che traforma il "risultato" in una matrice Latex
		var request= new XMLHttpRequest();
		//indico l'URI
		var requestURI= "js/latexConverter.php";
		var vars= "matrix="+input;
		//richiesta POST
		request.open('POST', requestURI, true);
		request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		request.responseType = 'text';
		request.onload = function(){
			if(request.readyState == 4 && request.status == 200) {
				document.getElementById(id).innerHTML= request.responseText;
				MathJax.Hub.Queue(["Typeset",MathJax.Hub,'id']); //ricarica lo script di MathJax per vedere le matrici in Latex

			}
		}
		request.send(vars); //invio variabili
	}
};
