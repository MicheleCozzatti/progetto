
<?php
//salvo nella variabile input ciò che viene passato dalla variabile superglobale $_POST
$input= $_POST["matrix"];
//trasformo la stringa JSON in un array
$arrayinput= json_decode(($input), true);
$matrice=$arrayinput["risultato"];

	
$latex2= ' ';
foreach($matrice as $key => $value){
	$latex='';
	foreach($value as $chiave =>$valore){
		//concateno ad ogni numero una &
		$latex.=$valore.'&';
	}
	//rimuovo l'ultima & 
	$latex= mb_substr($latex,0,-1);
	$separatore='\\\\';
	//aggiungo il separatore, che mi permette di definire la separazione tra una riga ed un'altra
	$latex2.=$latex.$separatore;
}
//rimuove i due separatori alla fine dell'ultima riga
$latex3 = mb_substr($latex2,1,-2);
$inizio='\begin{equation} A=\begin{vmatrix}';
$fine= '\end{vmatrix}\end{equation}';
//concateno la parte della matrice con le stringhe dell'inizio e della fine
$matrix=$inizio.$latex3.$fine;
echo $matrix;
?>