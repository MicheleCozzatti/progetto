#librerie utilizzate
import json
import logging
import numpy as np
import asyncio
import websockets

#funzione che calcola le varie operazioni utilizzando la libreria numpy
def calcolo(operazione, matrice1, matrice2=None):
	Controllo1= controllo(matrice1) #controllo gli elementi della matrice
	if(Controllo1=="errcontr"):
		return "errcontr"
	else:
		if(operazione=="Determinante"): #Determinante
			matr= np.array(matrice1)
			determinante= np.around(np.linalg.det(matr), decimals=4) #arrotondo
			risultato=determinante.item()
		elif(operazione=="Trasposta"): #Trasposta
			matr= np.array(matrice1)
			trasposta=np.round(matr.transpose(), decimals=4) #arrotondo
			risultato=trasposta.tolist()
		elif(operazione=="Inversa"): #Inversa
			matr= np.array(matrice1)
			determinante= np.linalg.det(matr)
			if(determinante==0): #Inversa, il det deve essere diverso da 0
				return "errinv"
			else:
				inversa=np.linalg.inv(matr)
				risultato= inversa.tolist()
		elif(operazione=="Rango"): #Rango
			matr=np.array(matrice1)
			rango= np.linalg.matrix_rank(matr)
			risultato=rango.item()
		elif(operazione=="Prodotto"): #Prodotto
			Controllo2=controllo(matrice2)
			if(Controllo2=="errcontr"): #controllo anche gli elementi della seconda matrice
				return "errcontr"
			else:
				matr1= np.array(matrice1)
				matr2= np.array(matrice2)
				risultato= np.dot(matr1, matr2)
		return risultato

#funzione che itera tutti gli elementi presenti nelle matrici e segnala errcontr quando c'è nè uno mancante o di formato non corretto	
def controllo(matrice):
	for riga in matrice:
		for el in riga:
			if(el == None):
				return "errcontr"				
				
@asyncio.coroutine
def websocketpy(websocket, path):
	inputJSON = yield from websocket.recv() #{"operazione":"Prodotto","matrice1":[[3,-4]],"matrice2":[[3,2,2],[2,-3,-3]]}
	#setto il file dove verranno scritti i messaggi, il livello ammesso e la data con formato
	logging.basicConfig(filename='control.log',level=logging.INFO, format='%(asctime)s; %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p')
	try:
		inputdict=json.loads(inputJSON) #trasformo input in formato JSON in un dizionario
		operazione=inputdict["operazione"]
		matrix1= inputdict["matrice1"]
		dictoutput={} #inizializzo dizionario vuoto che andrò a riempire con l'output che ottengo
		dictoutput["operazione"]=operazione
		listkey= len(list(inputdict.keys()))
		if (listkey==3): #quando le chiavi del mio dizionario sono formate dall'operazione, matrice1, matrice2
			matrix2=inputdict["matrice2"]
			result= calcolo(operazione, matrix1, matrix2)
			if(isinstance(result,str)==True): #controllo se la variabile result è una stringa, il risultato assume il formato stringa quando la funzione ritorna un errore
				dictoutput["risultato"]=result #se è una stringa la aggiungo al dizionario, altrimenti
			else:
				dictoutput["risultato"]=result.tolist() #aggiungo result in formato lista al dizionario
		else:
			result= calcolo(operazione, matrix1)
			dictoutput["risultato"]=result
		dictoutputJSON= json.dumps(dictoutput)
		#uso la libreria logging, printando il contenuto nel file control.log, printando sia input che output
		logging.info("input: "+ inputJSON+", output: "+dictoutputJSON) #21/05/2019 10:03:14 AM; INFO: input: {"operazione":"Determinante","matrice1":[[0,0,0],[0,0,0],[0,0,0]]}, output: {"operazione": "Determinante", "risultato": 0.0}
		res = dictoutputJSON
		yield from websocket.send(res)
	except:
		#caso eccezione
		errore="errJSON"
		dictoutput={} #inizializzo dizionario vuoto
		dictoutput["risultato"]= errore #aggiungo l'errore al dizionario vuoto
		dictoutputJSON= json.dumps(dictoutput) #trasformo il dizionario in una stringa JSON
		#uso la libreria logging per far printare un errore nel file control.log in caso di eccezione
		logging.error("input: "+ inputJSON+", output: "+dictoutputJSON) #21/05/2019 10:04:21 AM; ERROR: input: {"operazione":"Prodotto","matrice1":[[0],[0],[0]]}, output: {"risultato": "errJSON"}
		res = dictoutputJSON
		yield from websocket.send(res)
#inizializza il server
start_server = websockets.serve(websocketpy, 'localhost', 8765)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()

		
	