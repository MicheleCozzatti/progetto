# Matrix online  <img src="img/logo.jpg" width="50" height="50">
### 1. Introduzione
Il compito principale di questa applicazione è quello di effettuare le più importanti operazioni delle matrici che vengono normalmente utilizzati in diversi campi, dall’algebra    lineare, alla ricerca operativa. Questa applicazione è formata da una pagina iniziale, che attraverso dei bottoni rindirizzano all’operazione che l’utente vuole effettuare. 
<img src="imgsito/Home.PNG">

Le operazioni supportate sono: il prodotto tra due matrici, il rango di una matrice, il determinante, l’inversa e la trasposta. Ad ogni pagina poi è associata la definizione dell’operazione che si vuole effettuare ed una breve parte di teoria, nella quale vengono specificate le proprietà principali e in alcune vengono mostrati esempi di risoluzione.  Per la pagina iniziale, come per le altre pagine è stato utilizzato uno sfondo che dichiarato nel tag "body" ha l’effetto di fare da sfondo alle pagine. Le altre pagine invece oltre ad avere un altro sfondo sempre dichiarato nel tag "body" (uguale per tutte le pagine tranne per quella home) è stato definito un tag HTML “div”, che è un contenitore a cui è stato associato un color azzurro, che contiene la parte di teoria. Ho deciso di utilizzare questo metodo poiché la maggior parte delle pagine presenti nel web usano questo stile e anche per differenziare la visualizzazione dell’applicazione lato desktop e lato mobile. Quindi chi aprirà l’applicazione lato mobile vedrà solo il contenuto del contenitore azzurro, mentre per coloro che la apriranno lato desktop vedranno sia il contenitore azzurro e sia lo sfondo in background. Per quanto riguarda le matrici, ho voluto limitare la grandezza delle matrici fino ad un massimo di 5x5. Esse vengono generate dinamicamente da un file myjs.js. Per il determinante e inversa le matrici hanno l’obbligo di essere quadrate (righe e colonne uguali) mentre per la trasposta e il rango possono anche non essere quadrate. Per il prodotto il numero di colonne di una matrice deve essere uguale al numero di righe dell’altra. La icona che si trova in alto ad ogni pagina, tranne nella pagina principale Home, viene caricata da un link esterno, stessa cosa vale per il formato del testo e per le matrici che vengono rappresentate correttamente in Latex grazie alla libreria MathJax.
<img src="imgsito/det-desk-mobile.PNG">
### 2. Scopo dell'applicazione web
###### 1. Obbiettivo
L’obbiettivo principale di questa applicazione come già detto era quello di effettuare operazioni sulle matrici. 
###### 2. Requisiti di partenza
I requisiti dell’applicazione erano quelli di creare qualcosa che fosse in grado di effettuare delle operazioni tipiche delle matrici, ma che diversamente da altri siti dove producono solo un risultato, tenesse traccia delle proprietà principali delle operazioni e che offrisse qualche esempio in modo da invogliare anche l’utente a riprovare ad effettuare il calcolo.
###### 3. Use case
L’utente può scegliere dalla schermata Home quale operazione desidera effettuare attraverso il bottone. Una volta scelta definendo la dimensione della matrice, per inserire i dati deve cliccare il bottone “Imposta la matrice”. Fatto ciò la matrice verrà generata e per l’utente sarà possibile inserire i dati. Una volta popolata l’utente può premere il tasto calcola dove al di sotto della matrice verrà raffigurato il risultato.
###### 4. Funzionalità previste
Le funzionalità previste sono quelle di permettere all'utente di poter impostare le matrici in base alla dimensione scelta e fornire una base di teoria.

### 3. Progetto dell'applicazione web
###### 1. Definizione dei componenti e interazione tra loro
L’applicazione è composta da un file home.php che si occupa di richiamare le pagine presenti nel database, ad ogni pagina quindi è associato un ID che caratterizza una pagina dall’altra. Quindi quando verrà premuto un bottone per scegliere l’operazione da far effettuare, dalla pagina php verrà fatta partire attraverso PDO una query che interroga il database per quell’ID. In tutte le pagine è possibile inserire una matrice (per rango, determinante, trasposta e inversa), mentre due per il prodotto. Attraverso i tag "select"è possibile scegliere la dimensione della o delle matrici e un volta scelta, cliccando sul bottone "Imposta matrice", la matrice verrà generata. La matrice non è altro che una tabella HTML, che con opportuna funzione viene creata delle dimensioni impostati dai select e alla quale vengono aggiunti degli input i cui valori all’interno sono già preimpostati a zero.  Riempita la matrice l'utente potrà premere sul bottone "calcola" che apparirà solo quando la matrice sarà stata generata. Inizialmente il bottone non è visibile all'utente poichè è coperto attraverso la proprietà del css {visibility: hidden}. Si ricorda che gli input possono solo contenere valori numerici, l’inserimento di lettere da tastiera non è permesso attraverso un keypress, una volta che un tasto viene toccato, nel file .js una funzione controlla se il tasto è ammesso oppure no, ammettendo o impedendone l’inserimento. La matrice successivamente viene trasformata in un formato JSON, che viene inviata attraverso l’utilizzo di websocket ad il programma solver scritto nel linguaggio python che ritorna il risultato che viene poi mostrato all’utente. Nel caso del rango e del determinante verrà mostrato un numero mentre negli altri casi verrà mostrata una matrice in formato Latex. Questo è dovuto al fatto che una volta ricevuto il risultato dal file .py in formato JSON, questo viene inviato attraverso XMLhttpRequest ad un programma php che si occupa della conversione della matrice in formato Latex, che viene poi raffigurato nella pagina.
Per le pagine del determinante, inversa e anche per la colonna della prima matrice e la riga della seconda matrice per il prodotto, i valori all’interno dei select attraverso una funzione jQuery presente nello script all’interno della pagina, si muovono allo stesso tempo, ovvero che se un select cambia, anche l’altro cambia e assumerà lo stesso valore. Le pagine delle operazioni delle matrici sono composte da uno sfondo che è richiamato da una classe del foglio mycss, sopra questo è definito un tag "div" che ha un colore azzurro sempre definito dal css. Questo "div" funge da contenitore del contenuto delle pagine, che principalmente contengono la definizione dell’operazione, le varie proprietà e alcuni esempi. 
<img src="imgsito/Funzionamento.PNG">
### 4. Implementazione dell'applicazione web
###### 1. Tecnologie utilizzate
Le tecnologie utilizzate per questa applicazione sono:
1.	HTML 5;
2.	css;
3.	JavaScript;
4.	Python;
5.	PHP;
6.	JSON;
7.	JQuery;
8.	AJAX;
9.	WebSockets.

###### 2. Librerie esterne
Mentre le librerie esterne che sono state utilizzate:
1.	**Bootstrap**, libreria che permette di adattare le pagine alla dimensione della finestra, creando applicativi responsive. **Versione**:v4.3.1. **Licenza**:MIT license;
2.	**numpy**, libreria scientifica che utilizza array e particolari funzioni per effettuare calcoli sulle matrici.**Versione**:v1.15.4.**Licenza**:BSD license;
3.	**websockets**,libreria usata per generare una connessione full-duplex. **Versione**:v7.0. **Licenza**:BSD license
4.	**asyncio**, libreria utilizzata per scrivere codice usando la sintassi async / await. **Versione**:v3.4.3. **Licenza**:Apache license version 2.0;
5.	**json**, libreria utilizzata per codificare o decodificare le stringhe JSON. **Versione**:v2.0.9. **Licenza**:Python Software Foundation License;
6.	**logging**, libreria utilizzata nel file solver.py per riportare gli input e gli output ricevuti dal programma in un file .log, indicandoli con un grado di severità. **Versione**:0.5.1.2. **Licenza**:Python Software Foundation License.
7.	**MathJax**, libreria esterna utilizzata per rappresentare le matrici scritte in linguaggio latex. **Versione**:v2.0 . **Licenza**:Apache License(open-source)


###### 3. Descrizione codice 
L’applicazione è formata da diversi file di diverse tipologie di linguaggi:
1.	Home.php, questo file stabilisce la connessione con il database dove sono presenti le pagine, ad ogni pagina è associato un ID, questo viene catturato attraverso la variabile superglobale GET e poi inserita nella query che interroga il db attraverso l’ausilio di PDO.
2.	Solver.py, che si trova nella cartella py, si occupa di offrire un collegamento tra la parte javascript lato client e il programma py che effettua il calcolo, questo programma è formato principalmente da tre funzioni:
    1.	Websocketpy (websocket, path), è la funzione che si occupa della recezione del JSON proveniente da javascript lato client;
    2.	Calcolo(operazione, matrice1, matrice2=null), è una funzione che si occupa del calcolo vero e proprio delle diverse pagine, sono state utilizzate anche delle funzioni di arrotondamento per ottenere un risultato meno preciso, ma più presentabile;
    3.	Controllo(matrice), si occupa di controllare tutti gli elementi della matrice che gli vengono passati, in modo da ritornare un errore in tal caso un elemento non sia numerico.
3.	latexConverter.php, si occupa di trasformare la matrice JSON, che viene estrapolata dalla variabile superglobale GET e crea una matrice latex;
4.	myjs.js, è formata da ben 5 funzioni:
    1.	matrixDimension(n, corpo, nomepagina), dove n indica il numero del select utilizzato, corpo indica il corpo della tabella dove deve comparire la matrice, mentre nomepagina indica il nome dell’operazione scelta. Questa funzione una volta presi questi parametri crea la matrice nel corpo specificato, aggiungendo input che ammettono una lunghezza massima (maxlength) uguale a 5, che poi vengono aggiunti alle colonne, che a loro volta vengono aggiunte alle righe. Questa funzione gestisce anche il caso della copertura del bottone calcola, in modo da impedire che vengano inviati dati che possano causare errori al programma .py presente sul server;
    2.  isNumber(evt), dove evt è l’evento che viene passato come parametro legato alla pressione di un tasto (onkeypress). Questa funzione previene il fatto che i dati inseriti negli input siano solo numerici;
    3.	takeData(nomepagina,corpo, corpo2=null), nomepagina si riferisce al nome della pagina presa in considerazione , corpo indica l’ID del corpo della tabella della matrice, corpo2, non è altro che il secondo corpo, nel caso del prodotto, dove le matrici sono due, quindi non è altro che il corpo della tabella della seconda matrice. Questa funzione si occupa di prendere i dati da queste tabelle e restituirne un JSON;
    4.	drawMatrix(input,id), dove input è l’output del programma .py e id è l’identificativo del tag dove il risultato deve essere raffigurato, nel caso del determinante e rango dove il risultato restituito sarà un numero. Mentre per prodotto, trasposta, inversa, il risultato deve essere una matrice. Quindi per queste viene fatta una richiesta ad il file latexConverter.php che trasforma la matrice in formato JSON in una matrice raffigurata con Latex. Con una richiesta XMLHttpRequest viene estratta la matrice in latex e raffigurata sulla pagina web.
    5.	runWebSocket(n,nomepagina,corpo,corpo2=null), che viene richiamata quando l'utente preme il bottone calcola che richiama a sua volta le funzioni takeData per prendere i dati e inviarli al file python e una volta ottenuto il risultato richiama drawMatrix per raffigurarla nella pagina.
5.	mycss.css, contiene la formattazione del testo delle pagine, definendone alcune proprietà;
6.	Nei tag script interni alle pagine determinante, inversa e prodotto è stata creata una funzione che permette di cambiare nello stesso tempo due select in modo che abbiano stessi valori. Questa funzione è stata creata per prevenire errori sul server, poiché per calcolare il determinante e l’inversa la matrice deve essere quadrata, mentre per il prodotto, il numero di colonne della prima matrice deve essere uguale al numero di colonne della seconda.

###### 4. Cronoprogramma iniziale ed effettivo
Per la realizzazione di questa applicazione, inizialmente avrei stimato un numero di ore oltre a 70, mentre realmente il numero di ore dedicato a questo progetto è superiore alle 100 ore. La maggior parte del lavoro è stata dedicata per la parte grafica usando il CSS e la parte di javascript.
<table>
    <tbody>
        <tr>
            <td> <strong>Elenco attività<strong></td>
            <td> <strong>Tempo stimato (ore)</strong> </td>
            <td> <strong>Tempo effettivo (ore)</strong></td>
        </tr>
        <tr>
            <td> Scrittura HTML </td>
            <td> 20 </td>
            <td> 25 </td>
        </tr>
        <tr>
            <td> Scrittura css</td>
            <td> 10</td>
            <td> 15</td>
        </tr>
         <tr>
            <td> Scrittura javaScript</td>
            <td> 20</td>
            <td> 30</td>
        </tr>
         <tr>
            <td> Scrittura PHP</td>
            <td> 10 </td>
            <td> 15 </td>
        </tr>
         <tr>
            <td> Scrittura programma python</td>
            <td> 10</td>
            <td> 20</td>
        </tr>
         <tr>
            <td> Progettazione DB</td>
            <td> 7</td>
            <td> 3</td>
        </tr>
         <tr>
            <td> <strong>Totale</strong></td>
            <td> <strong>77</strong></td>
            <td> <strong>108</strong></td>
        </tr>
    </tbody>
</table>

### 5. Conclusioni
Questa applicazione funziona per le diverse operazioni, sicuramente nel futuro può essere espansa aggiungendo la possibilità di aggiungere anche delle incognite, specialmente nel determinante. Ad esempio, una retta passante per due punti si può trovare calcolando il determinante di una matrice 3x3, ponendo ogni elemento sulla terza colonna uguale ad 1. 

