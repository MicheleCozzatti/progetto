<?php
//connessione al db
$host = '127.0.0.1';
$db = 'dbsito';
$user = 'root';
$pass = '';
$charset = 'utf8mb4';
$options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, ];
$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
//gestisco errori
try {
    $pdo = new PDO($dsn, $user, $pass, $options);
}
catch(\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int)$e->getCode());
}
$pag = 6; //pagina iniziale con id=6
if (isset($_GET['id'])) {
    $val = (int)$_GET['id'];
    //il valore delle pagine è compreso tra 1 e 6 e deve essere intero
    if (is_int($val) && $val > 0 && $val < 7) {
        $pag = $val;
    }
}
//preparo lo statement
$stmt = $pdo->prepare('SELECT testo FROM teoria WHERE id= ?');
//inserisco i paramentri nella execute
$stmt->execute([$pag]);
//estraggo tutte le righe disponibili nello statement, in questo caso andrò a caricare tutte le righe della pagina corrente presente nel db.
while ($row = $stmt->fetch()) {
    echo $row['testo'] . "<br>";
}
?>
